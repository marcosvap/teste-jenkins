package com.teste.claro.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class EndpointsController {

	@RequestMapping(value = "endpoint/hello", method = RequestMethod.GET, produces = "application/json")
	public String hello() throws Exception {
		return "Hello";

	}

}